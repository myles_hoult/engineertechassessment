<!DOCTYPE html>
<html>
<head>
<title>Pet of the day</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", Arial, Helvetica, sans-serif}
</style>
</head>
<?php Require_once "config.php" ?>
<body class="w3-light-grey">

<!-- Navigation Bar -->
<div class="w3-bar w3-white w3-large">
  <a href="Index.php" class="w3-bar-item w3-button w3-mobile">Home</a>
  <a href="TodaysPet.php" class="w3-bar-item w3-button w3-mobile">Pet Of the Day</a>
  <a href="Interest.php" class="w3-bar-item w3-button w3-mobile">Expression Of intrest!</a>
</div>

<div class="w3-content" style="max-width:1532px;">
<h1>Todays Good Boy/Girl </h1>
<div class="w3-center w3-green">
<?php
$random = rand(1,5);
          // selects all menu items if the item type is a main menu dish
$sql = "SELECT * FROM dogs WHERE id = $random";
$result = $link->query($sql);
// if there is are more than 0 rows....
if ($result->num_rows > 0) {
 // ....output data of each row
  while($row = $result->fetch_assoc()) {?>
  <!-- formatting for output data -->
  <?php echo "<p>" . "<b>" . "<h1> ". "Breed: " .  $row["breed"] . "</h1>" . "</p>"; ?>
    <img src=<?php
    echo $row['image']?>
                    alt=<?php
                    echo $row['breed']?> class="w3-round w3-image "><?php
    echo "<p>" . "Sub Breed: " . $row["subBreed"] . "</p>" . "<b>" . "Related breeds: " . $row["relatedBreed"] . "</p>";
  }
} else {
  //if no data in the table matched the sql query, display this message
  echo "There are no good dogs today :(";
}
?>
<a class="w3-button w3-round w3-black" href="TodaysPet.php">Refresh</a>
</div>
</div>

</body>
</html>