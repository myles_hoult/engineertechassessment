<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", Arial, Helvetica, sans-serif}
</style>
</head>
<body class="w3-light-grey">

<!-- Navigation Bar -->
<div class="w3-bar w3-white w3-large">
  <a href="Index.php" class="w3-bar-item w3-button w3-mobile">Home</a>
  <a href="TodaysPet.php" class="w3-bar-item w3-button w3-mobile">Pet Of the Day</a>
  <a href="Interest.php" class="w3-bar-item w3-button w3-mobile">Expression Of intrest!</a>
</div>
<!-- Header -->
<h1 class="w3-center"><b>Pet Sitter Expression of Interest</h1>
<header class="w3-display-container w3-content" style="max-width:1500px;">
  <img class="w3-image" src="Pictures/dog.jpg" alt="dog" style="min-width:1000px" width="1600" height="500">
  <div class="w3-display-left w3-padding w3-col l6 m8">
    <div class="w3-container w3-green">
      <h1>Mad Paws - Slogan</h1>
    </div>
    
</header>

<!-- Page content -->
<div class="w3-content" style="max-width:1532px;">
<h2>FAQ's</h2>
  <div class="w3-third" >
      <h3><b>Can you trust Pet sitters?</b></h3>
      <h6>At Mad Paws, we know it can be hard to entrust your pet’s welfare to someone else. After all, they’re not just a dog, cat, puppy, rabbit, guinea pig, or bird – they’re a part of your family. It’s for this reason that Mad Paws takes Pet Sitting so seriously.</h6>
      <h6>All of our Pet Sitters undergo a thorough vetting procedure before they appear on Mad Paws. In fact, of all our Pet Sitting applicants, only one in four are accepted to become Mad Paws Pet Sitters. If you’re still not sure about a Pet Sitter, you can also read reviews from previous clients. By doing so, you can learn more about their pet care style from other Pet Owners.</h6>
    </div>
    <div class="w3-third" >
      <h3><b>What can you expect from a Mad Paws Pet Sitter?</h3>
      <h6>Of course, you don’t just want someone who is going to keep your pet safe. Rather, you want an experienced pet lover who will pamper your furry friend while you’re away. That’s why Mad Paws expects the very best from our Pet Sitters.</h6>
      <h6>When you book a Pet Sitter through Mad Paws, you’re not booking an impersonal dog boarding kennel or cattery. You’re leaving your furry friend in the hands of a certified, verified, bona fide pet lover. Instead of simply passing the time until you return, your pet will feel like they’re on a holiday themselves. Unlike many kennels and pet hotels, Mad Paws’ Pet Sitting prices are also very affordable. In other words, your pet will enjoy a higher quality of care for a lower price!</h6>
    </div>
    <div class="w3-third" >
      <h3><b>Can you meet a Pet Sitter before making a booking?</h3>
      <h6>Once you’ve found a Pet Sitting candidate through Mad Paws, you can arrange a Meet & Greet. This gives you a forum to meet your potential Pet Minder in person and ensure they’re perfect for your animal. Just as integrally, your pet will have the chance to “interview” the human who might be caring for them! Still not 100%? Why not book your pet in for an overnight trial stay with their Pet Sitter?</h6>
      <h6>After the Meet & Greet, if you decide the Pet Sitter is ideal for your furbaby, you can confirm the booking. While you’re away, your Pet Boarding pro will keep you updated with photos and messages.</h6>
      <h6>From Sydney to Perth, from Brisbane to Melbourne, you’ll find the best in Australian Pet Sitting with Mad Paws. Contact a Mad Paws Pet Sitter today and give your furry friend a top-tier Pet Boarding experience!</h6>
    </div>
    <div class="w3-third" >
      <h3><b>What is Pet sitting?</h3>
      <h6>Pet Sitting is a service through which a pet care professional minds your dog, cat, puppy, bird, or other pet. Depending on the preferences of the Pet Owner, Pet Sitting can occur either at the Owner’s or the Sitter’s house. In the former case, Pet Sitters can offer Pet House Sitting, by which they mind your home and your furbaby.</h6>
      <h6>Pet Sitting can span for as long as the Pet Owner needs, from overnight to months at a time. Pet Sitting is also sometimes known as Dog Boarding, Cat Boarding, or Pet Boarding.</h6>
      <h6>Wondering how to find a Pet Sitter? Simply search your local suburb or area for pet care professionals using Mad Paws’ search widget. Once you’ve found the right Pet Minder, you can create a Mad Paws account and contact the Sitter in question.</h6>
    </div>
    <div class="w3-third" >
      <h3><b>How much is Pet Sitting per hour? How much to pay a Pet Sitter overnight?</h3>
      <h6>Pet Sitting prices can vary between Pet Sitters. Each Pet Sitter sets their own fees, with some prices being as low as $16 for an overnight stay. For the Pet Sitting option that meets your budget, browse the list of Pet Sitters in your area.</h6>
      </div>
    <div class="w3-third" >
      <h3><b>How to become a Pet Sitter? How to make money Pet Sitting?</h3>
      <h6>Are you interested in knowing how to become a certified Pet Sitter? To begin the process, all you need to do is create a Mad Paws account. You can do this by clicking “Become a Sitter” in the top right corner of the home page.</h6>
      <h6>From there, we will guide you through our online training program and four-step verification process. After you’ve successfully completed this, you’ll be able to create a Pet Sitting profile, which will allow you to offer Pet House Sitting, Dog Boarding, Dog Walking, Pet Day Care, and many other services.</h6>
      <h6>As a Pet Sitting professional, you will choose which services you offer. You can also decide whether you want to care for pets in your home or the pet’s home. Finally, you get to choose how much you charge for your Pet Sitting prices.</h6>
      <h6>When your profile is ready, you can start accepting bookings and making money!</h6>
    </div>
    <div class="w3-third" >
      <h3><b>Do Pet Sitters need accident cover?</h3>
      <h6>Mad Paws covers all bookings made through our platform with Mad Paws Accident Cover. It covers the pet being cared for in the event of an expected problem. Should a Pet Sitter wish to cover themselves, their homes, or their own pets, we suggest they obtain additional insurance.<h6>
<!-- End page content -->
</div>


</body>
</html>
