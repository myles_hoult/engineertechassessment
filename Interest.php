<!DOCTYPE html>
<html>
<head>
<title>Expression of Interest</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", Arial, Helvetica, sans-serif}
</style>
</head>
<body class="w3-light-grey">

<!-- Navigation Bar -->
<div class="w3-bar w3-white w3-large">
  <a href="Index.php" class="w3-bar-item w3-button w3-mobile">Home</a>
  <a href="TodaysPet.php" class="w3-bar-item w3-button w3-mobile">Pet Of the Day</a>
  <a href="Interest.php" class="w3-bar-item w3-button w3-mobile">Expression Of intrest! </a>
</div>
<div class="w3-container">
    <h2>To get Involved fill in the form below</h2>
    <form onsubmit="myFunction()">
      <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Name" required name="Name"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="number" placeholder="Age" required name="Age"></p>
     <p> <input type="checkbox" required name = "preference" id="dog"> <label for ="dog">I'm a Dog owner!</label>
      <input type="checkbox" required name = "preference" id="cat"> <label for ="cat">I'm a Cat owner!</label></p>
      <label for="states">Choose your area:</label>

<select name="area" id="states">
<option value="">Choose</option>
  <option value="New South Wales">New South Wales</option>
  <option value="Victoria">Victoria</option>
  <option value="Queensland">Queensland</option>
  <option value="Western Australia">Western Australia</option>
  <option value="South Australia">South Australia</option>
  <option value="Tasmania">Tasmania</option>
  <option value="Nothern Territory">Nothern Territory</option>
  <option value="Australian Capital Territory">Australian Capital Territory</option>
</select>

      <p><button class="w3-button w3-black w3-padding-large" type="submit" value ="submit">Submit Your form!</button></p>
    </form>
  </div>
  <script>
function myFunction() {
  alert("Your form has been submitted");
}
</script>

  </body>
</html>