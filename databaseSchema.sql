Drop table if exists dogs;
Drop table if exists people;

create table dogs(
id int auto_increment primary key,
breed varchar(45),
subBreed varchar(45),
relatedBreed Varchar(45),
image varchar(250)
);

insert into dogs (breed, subBreed, relatedBreed, image)
VALUES ('Hound', 'English Hound', 'Afghan Hound', 'https://images.dog.ceo/breeds/hound-english/n02089973_3933.jpg');
insert into dogs (breed, subBreed, relatedBreed, image)
VALUES ('Hound', 'Walker Hound', 'English Hound', 'https://images.dog.ceo/breeds/hound-walker/n02089867_1882.jpg');
insert into dogs (breed, subBreed, relatedBreed, image)
VALUES ('Hound', 'Afghan Hound', 'Walker Hound', 'https://images.dog.ceo/breeds/hound-afghan/n02088094_1724.jpg');
insert into dogs (breed, subBreed, relatedBreed, image)
VALUES ('Hound', 'Ibizan Hound', 'Blood Hound', 'https://images.dog.ceo/breeds/hound-ibizan/n02091244_5488.jpg');
insert into dogs (breed, subBreed, relatedBreed, image)
VALUES ('Hound', 'Blood Hound', 'Ibizan Hound', 'https://images.dog.ceo/breeds/hound-blood/n02088466_7606.jpg');

create table people(
id int auto_increment primary key,
`name` varchar(45),
age int(99),
preference varchar(3),
area varchar(45)
)
select * from dogs;