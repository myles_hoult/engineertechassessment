<?php
// define the information about the database
define('DB_SERVER', '127.0.0.1');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'pets');

// connect to the databse
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

// Check if there is a connection
if ($link === false) {
    die("ERROR: Could not connect to the mySQL server, please try again. " . mysqli_connect_error());
}

?>